import sys

import NumberSet

class Card():
    def __init__(self, idnum, size, numberSet):
        """Card constructor"""
        self.__idnum = idnum
        self.__size = size
        numberSet.randomize()
        self.__numberSet = numberSet
        self.__cardList = []
        for i in range(size * size):
            self.__cardList.append(numberSet.getNext())


    def getId(self):
        """Return an integer: the ID number of the card"""
        return self.__idnum

    def getSize(self):
        """Return an integer: the size of one dimension of the card.
        A 3x3 card will return 3, a 5x5 card will return 5, etc.
        """
        return self.__size

    def print(self, file=sys.stdout):
        """void function:
        Prints a card to the screen or to an open file object"""
        isOdd = (self.__size % 2 ==1)

        for i in range(self.__size):
            print("+", end="", file=file)
            for j in range(self.__size):
                print("------+", end="", file = file)
            print("\n|", end="", file = file)
            for j in range(self.__size):
                block = j + (self.__size*i)
                if block == (self.__size**2//2) and isOdd:
                    print("FREE!|", end="", file = file)
                else:
                    print("{:^5d}|".format(self.__cardList[j+(i*self.__size)]), end="", file = file)
        print("\n+", end="", file = file)
        for j in range(self.__size):
            print("------+", end="", file=file)

