import random

class NumberSet():
    def __init__(self, size):
        self.__currentIndex = 0
        self.__size = size
        self.__NumberSet = []
        for i in range(size):
            self.__NumberSet.append(i+1)


    def getSize(self):
       return self.__size

    def get(self, index):
        """Return an integer: get the number from this NumberSet at an index"""
        if index >= self.__size:
            return None
        return self.__NumberSet[index]



    def randomize(self):
        """void function: Shuffle this NumberSet"""
        random.shuffle(self.__NumberSet)


    def getNext(self):
        """Return an integer: when called repeatedly return successive values
        from the NumberSet until the end is reached, at which time 'None' is returned"""
        if self.__currentIndex < len(self.__NumberSet):
            card = self.__NumberSet[self.__currentIndex]
            self.__currentIndex += 1
            return card
        else:
            return None
